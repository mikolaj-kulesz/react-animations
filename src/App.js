import React, {Component} from 'react';
import Transition from 'react-transition-group/Transition';
import CSSTransition from 'react-transition-group/CSSTransition';

import './App.css';
import Modal from './components/Modal/Modal';
import Backdrop from './components/Backdrop/Backdrop';
import List from './components/List/List';

class App extends Component {
    state = {
        modalIsOpen: false,
        modalIsOpen2: false,
        showBlock: false,
    };

    showModal = () => {
        this.setState({modalIsOpen: true});
    };

    closeModal = () => {
        this.setState({modalIsOpen: false});
    };

    showModal2 = () => {
        this.setState({modalIsOpen2: true});
    };

    closeModal2 = () => {
        this.setState({modalIsOpen2: false});
    };


    toggleBlock = () => {
        const state = !this.state.showBlock;
        this.setState({showBlock: state});
    };

    block = (
        <div style={{
            background: 'red',
            height: 200,
            width: 200,
            margin: '10px auto'
        }}>red block</div>
    );

    animationTiming = {
        enter: 1000,
        exit: 1000
    }

    render() {
        return (
            <div className="App">
                <h1>React Animations</h1>
                <button onClick={() => this.toggleBlock()}>Toggle</button>
                <Transition
                    in={this.state.showBlock}
                    timeout={500}
                    mountOnEnter
                    unmountOnExit
                    onEnter={() => console.log('onEnter')}
                    onEntering={() => console.log('onEntering')}
                    onEntered={() => console.log('onEntered')}
                    onExit={() => console.log('onExit')}
                    onExiting={() => console.log('onExiting')}
                    onExited={() => console.log('onExited')}
                >
                    {state => (
                        <div style={{
                            background: 'red',
                            height: 200,
                            width: 200,
                            margin: '10px auto',
                            transition: 'all 0.3s ease-in-out',
                            opacity: state === 'exiting' ? 0 : 1,
                        }}>red block</div>
                    )}
                </Transition>
                <Transition
                    in={this.state.modalIsOpen}
                    timeout={this.animationTiming}
                    mountOnEnter
                    unmountOnExit
                    onEnter={() => console.log('onEnter')}
                    onEntering={() => console.log('onEntering')}
                    onEntered={() => console.log('onEntered')}
                    onExit={() => console.log('onExit')}
                    onExiting={() => console.log('onExiting')}
                    onExited={() => console.log('onExited')}>
                    {state => (
                        <Modal show={state} closed={this.closeModal}/>
                    )}
                </Transition>
                <CSSTransition
                    in={this.state.modalIsOpen2}
                    timeout={this.animationTiming}
                    mountOnEnter
                    unmountOnExit
                    onEnter={() => console.log('onEnter')}
                    onEntering={() => console.log('onEntering')}
                    onEntered={() => console.log('onEntered')}
                    onExit={() => console.log('onExit')}
                    onExiting={() => console.log('onExiting')}
                    onExited={() => console.log('onExited')}
                    classNames="vertical-slide">
                    {state => (
                        <Modal show={state} closed={this.closeModal2}/>
                    )}
                </CSSTransition>
                <br/>
                {this.state.showBlock ? this.block : null}
                <Backdrop show={this.state.modalIsOpen}/>
                <Backdrop show={this.state.modalIsOpen2}/>
                <button className="Button" onClick={this.showModal}>Open Modal</button>
                <br/>
                <br/>
                <button className="Button" onClick={this.showModal2}>Open Modal2</button>
                <h3>Animating Lists</h3>
                <List/>
            </div>);
    }
}

export default App;
